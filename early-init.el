;;; early-init.el --- Early init configuration.

;;; Commentary:
;; Use for early-init-specific options only.

;;; Code:

(setq-default
 ;; Disable package.el
 package-enable-at-startup nil
 ;; Setup default frame options
 default-frame-alist '((fullscreen . maximized)
		       (undecorated . t)))

;; Disable useless parts of interface
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)

(provide 'early-init)
;;; early-init.el ends here.
