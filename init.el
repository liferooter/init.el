;;; init.el -- My Emacs configuration. -*- eval: (outline-hide-body); -*-

;;; Commentary:
;; It uses Outshine mode for structure.

;;; Code:

;;;; Customizable options

(defvar my/font-name "JetBrains Mono"
  "My font name.")

(defvar my/font-size 13
  "My font size.")

(defvar my/doom-theme 'doom-monokai-pro
  "My Doom theme.")

;;;; Setup package system
;;;;; Move Emacs home to xdg-data
(require 'xdg)
(setq user-emacs-directory
  (expand-file-name "emacs" (xdg-data-home)))

;;;;; Setup straight.el
(setq-default straight-use-package-by-default t
	      straight-base-dir (locate-user-emacs-file "packages")
	      straight-repository-branch "develop"
	      straight-check-for-modifications '(watch-files
						 find-when-checking))

;; Bootstrap straight.el
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" straight-base-dir))
      (bootstrap-version 6))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

;;;;; Install use-package
(straight-use-package 'use-package)

;;;; Core setup
;;;;; Setup Emacs core
(use-package emacs
  :demand t
  :no-require t
  :straight nil
  
  :hook
  ;; Delete selection on typing
  (after-init . delete-selection-mode)
  ;; Autocomplete parentheses
  (after-init . electric-pair-mode)
  ;; Revert changes when file is changed on disk
  (after-init . global-auto-revert-mode)
  ;; Disable autosave
  (after-init . (lambda ()
                  (auto-save-mode -1)))
  ;; Setup server
  (after-init . server-mode)
  
  ;; Track recent files
  (after-init . recentf-mode)

  
  ;; Add basic prog-mode features
  (prog-mode  . display-line-numbers-mode)
  (prog-mode  . hl-line-mode)
  
  :custom
  (read-process-output-max      (* 1024 1024))              ;; Increase process read buffer size
  
  (custom-file                  (locate-user-emacs-file
                                 "custom.el"))              ;; Move custom file to data home
  
  (make-backup-files            nil)                        ;; Disable backups
  (create-lockfiles             nil)                        ;; Disable lockfiles
  
  (use-short-answers            t)                          ;; Accept 'y' instead of 'yes'
  
  (column-number-mode           t)                          ;; Display column number
  (enable-recursive-minibuffers t)                          ;; Enable recursive minibuffers
  (cursor-type                  'bar)                       ;; Set bar-style cursor
  (indent-tabs-mode             nil)                        ;; Use spaces instead of tabs
  (use-dialog-box               nil)                        ;; Disable graphical dialogs
  (inhibit-startup-screen       t)                          ;; Disable startup screen even when dashboard is not shown

  (warning-minimum-level       :error)                      ;; Disable warnings

  :bind
  ;; Recent navigations
  ("C-r" . recentf-open-files)
  ("C-p" . project-switch-project)

  ;; Smart home
  ("<home>" . (lambda ()
	        "Jump to indent or to beginning of line."
	        (interactive "^")
	        (let ((current-point (point)))
		  (back-to-indentation)
		  (when (eq current-point (point))
		    (move-beginning-of-line 1)))))
  
  :config
  ;; Make Escape key work as C-g
  (when (display-graphic-p)
    (define-key key-translation-map (kbd "ESC") (kbd "C-g")))

  ;; Set default font
  (add-to-list 'default-frame-alist
	       `(font . ,(format "%s-%d" my/font-name my/font-size)))

  ;; Enable mouse support in terminal
  (unless (display-graphic-p)
    (xterm-mouse-mode 1))

  ;; Load custom file
  (load custom-file :noerror))

;;;;;; Setup garbadge collection
(use-package gcmh
  :hook after-init)

;;;;;; Setup autosave
(use-package super-save
  :hook after-init)

;;;;; Setup user interface

;; Doom themes
(use-package doom-themes
  :config
  (load-theme my/doom-theme t)
  (doom-themes-visual-bell-config)
  (doom-themes-org-config))

;; Dashboard
(use-package dashboard
  :custom
  (dashboard-set-init-info     t)
  (dashboard-startup-banner    'logo)
  (dashboard-center-content    t)
  (dashboard-set-navigator     t)
  (dashboard-items             '((recents   . 5)
				 (projects  . 5)
				 (bookmarks . 5)))
  (dashboard-projects-backend 'project-el)
  
  :config
  (dashboard-setup-startup-hook))

;; Modeline
(use-package mood-line
  :config
  (mood-line-mode))

;;;;; Enable font ligatures
(use-package ligature
  :hook
  (after-init . global-ligature-mode)
  :config
  (defvar my/ligatures-all '("-|" "-~" "---" "-<<" "-<" "--" "->" "->>" "-->" "///" "/=" "/=="
                            "/>" "//" "/*" "*>" "***" "*/" "<-" "<<-" "<=>" "<=" "<|" "<||"
                            "<|||" "<|>" "<:" "<>" "<-<" "<<<" "<==" "<<=" "<=<" "<==>" "<-|"
                            "<<" "<~>" "<=|" "<~~" "<~" "<$>" "<$" "<+>" "<+" "</>" "</" "<*"
                            "<*>" "<->" "<!--" ":>" ":<" ":::" "::" ":?" ":?>" ":=" "::=" "=>>"
                            "==>" "=/=" "=!=" "=>" "===" "=:=" "==" "!==" "!!" "!=" ">]" ">:"
                            ">>-" ">>=" ">=>" ">>>" ">-" ">=" "&&&" "&&" "|||>" "||>" "|>" "|]"
                            "|}" "|=>" "|->" "|=" "||-" "|-" "||=" "||" ".." ".?" ".=" ".-" "..<"
                            "..." "+++" "+>" "++" "[||]" "[<" "[|" "{|" "??" "?." "?=" "?:" "##"
                            "###" "####" "#[" "#{" "#=" "#!" "#:" "#_(" "#_" "#?" "#(" ";;" "_|_"
                            "__" "~~" "~~>" "~>" "~-" "~@" "$>" "^=" "]#"))
  (ligature-set-ligatures 'emacs-lisp-mode
			  '("..." ".." ";;"))
  (ligature-set-ligatures 'rust-mode
			  my/ligatures-all))

;;;;; Enable solaire-mode
(use-package solaire-mode
  :hook
  (after-init . solaire-global-mode))

;;;;; Show page breaks
(use-package page-break-lines
  :hook
  (after-init . global-page-break-lines-mode))

;;;; Setup programming mode

;;;;; Highlight changes
(use-package diff-hl
  :hook
  (after-init . global-diff-hl-mode)
  (after-init . diff-hl-flydiff-mode))

;;;;; Save place in buffer
(use-package saveplace
  :hook (after-init . save-place-mode))

;;;;; Enable linting
(use-package flycheck
  :hook prog-mode
  
  :custom-face
  ;; Make error ID less creepy
  (flycheck-error-list-id-with-explainer ((t (:inherit flycheck-error-list-id))))

  :custom
  (flycheck-indication-mode nil)
  
  :config
  (add-to-list 'display-buffer-alist
               '((lambda (buffer-or-name _)
                   (let ((buffer (get-buffer buffer-or-name)))
                     (with-current-buffer buffer
                       (or (equal major-mode 'flycheck-error-list-mode)
			   (string-prefix-p "*Flycheck errors*" (buffer-name buffer))))))
                 (display-buffer-reuse-window display-buffer-in-side-window)
                 (side . bottom)
                 (reusable-frames . visible)
                 (window-height . 0.3))))

;;;;; Enable Outshine for Lisp buffers
(use-package outshine
  :hook emacs-lisp-mode
  :bind (:map outshine-mode-map
              ("C-<return>" . outline-toggle-children))
  :custom (outline-minor-mode-prefix (kbd "C-c #")))

;;;;; Enable autocompletion
;; Enable Corfu
(use-package corfu
  :custom
  (corfu-auto             t)
  (corfu-quit-at-boundary t)
  (corfu-quit-no-match    t)
  (corfu-auto-delay       0.0)
  (corfu-preview-current  nil)
  :hook
  (after-init . global-corfu-mode)
  :straight '(corfu
	      :files (:defaults "extensions/*")
	      :includes (corfu-history)))

;; ...in terminal
(use-package corfu-terminal
  :if (not (display-graphic-p))
  
  :hook corfu-mode)

;; Enable Corfu Doc
(use-package corfu-doc
  :bind (:map corfu-map
	      ("M-<down>" . corfu-doc-scroll-up)
	      ("M-<up>" . corfu-doc-scroll-down))
  :hook corfu-mode)

;; ...in terminal
(use-package corfu-doc-terminal
  :if (not (display-graphic-p))
  
  :hook corfu-doc-mode
  
  :straight '(corfu-doc-terminal
	      :type git
	      :repo "https://codeberg.org/akib/emacs-corfu-doc-terminal.git"))

;;;;; Enable snippets
(use-package yasnippet
  :hook (prog-mode . yas-minor-mode)
  :bind (:map yas-minor-mode-map
	      ("M-+" . yas-expand)))

;; Use a lot of pre-defined snippets
(use-package yasnippet-snippets
  :after yasnippet)

;;;;; Setup terminal
(use-package vterm-toggle
  :bind
  ("C-`" . vterm-toggle)
  
  :custom
  (vterm-toggle-fullscreen-p         nil)
  (vterm-toggle-use-dedicated-buffer nil)
  (vterm-toggle-reset-window-configration-after-exit nil)
  (vterm-toggle-scope 'project)
  
  :config
  (add-to-list 'display-buffer-alist
               '((lambda (buffer-or-name _)
                   (let ((buffer (get-buffer buffer-or-name)))
                     (with-current-buffer buffer
                       (or (equal major-mode 'vterm-mode)
			   (string-prefix-p "*vterm*" (buffer-name buffer))))))
                 (display-buffer-reuse-window display-buffer-in-side-window)
                 (side . right)
                 (reusable-frames . visible)
                 (window-height . 0.5))))

;;;; Setup LSP

;;;;; Enable LSP mode
(use-package lsp-mode
  :preface (setq-default lsp-use-plists t)

  :hook
  ;; Run for supported languages
  (rust-mode . lsp)
  
  :custom
  (lsp-keymap-prefix "C-c l")
  (lsp-completion-provider :none)
  (lsp-diagnostics-provider :flycheck)
  (lsp-idle-delay 0.200)
  (lsp-headerline-breadcrumb-enable nil)
  (lsp-modeline-code-actions-segments '(count name))
  (lsp-signature-doc-lines 1)
  
  :bind
  (:map lsp-mode-map
	("C-." . lsp-execute-code-action))
  :config
  (add-to-list 'lsp-file-watch-ignored "/target")
  :commands (lsp lsp-deferred))

;;;; Languages support

;;;;; Rust support
(use-package rustic
  :mode ("\\.rs\\'" . rustic-mode)
  :custom
  (rustic-format-on-save t)
  (rustic-format-on-save-method 'lsp-format-buffer))

;;;; Setup Vertico

;;;;; Enable Vertico
(use-package vertico
  :hook
  after-init
  (vertico-mode . vertico-mouse-mode)
  (minibufer-setup . cursor-intangible-mode)

  :custom-face
  (minibuffer-prompt ((t (:inherit font-lock-keyword-face :bold t))))
  
  :custom
  ;; Setup minibuffer prompt
  (minibuffer-prompt-properties
   `(read-only t cursor-intangible t face minibuffer-prompt))

  :straight `(vertico
	      :files (:defaults "extensions/*")
	      :includes (vertico-mouse)))

;;;;; Persist history over Emacs restarts. Vertico sorts by history position.
(use-package savehist
  :hook after-init)

;;;;; Enable orderless completion style
(use-package orderless
  :custom
  (completion-styles '(orderless
		       basic
		       partial-completion))
  (completion-category-defaults nil)
  (completion-category-overrides '((file (styles partial-completion)))))

;;;;; Enable rich annotations in minibuffer
(use-package marginalia
  :hook after-init)

;;;; Setup Consult
;;;;; Enable consult
(use-package consult
  :demand t
  :custom
  (xref-show-xrefs-function       'consult-xref)
  (xref-show-definitions-function 'consult-xref)
  (consult-narrow-key "<")
  
  :bind
  ([remap switch-to-buffer]              . consult-buffer)
  ([remap switch-to-buffer-other-window] . consult-buffer-other-window)
  ([remap switch-to-buffer-other-frame]  . consult-buffer-other-frame)
  ([remap project-switch-to-buffer]      . consult-project-buffer)
  ([remap bookmark-jump]                 . consult-bookmark)
  ([remap yank-pop]                      . consult-yank-pop)
  ([remap goto-line]                     . consult-goto-line)
  ([remap recentf-open-files]            . consult-recent-file)
  ([remap isearch-forward]               . (lambda ()
					     (interactive)
					     (consult-line (when mark-active
							     (buffer-substring-no-properties (region-beginning)
											     (region-end))))))

  ("C-c f f" . consult-find)
  ("C-c f g" . consult-ripgrep)
  ("C-h M" . consult-man)
  ("M-g o" . consult-outline)
  ("M-g i" . consult-imenu)
  ("M-s F" . consult-locate)
  ("M-s g" . consult-ripgrep)
  ("M-s G" . consult-git-grep)
  
  (:map isearch-mode-map
	([remap isearch-edit-string] . consult-isearch-history))
  (:map minibuffer-local-map
	([remap next-matching-history-element] . consult-history)
	([remap previous-matching-history-element] . consult-history)))

;;;;; Enable Flycheck integration
(use-package consult-flycheck
  :bind
  (:map flycheck-mode-map
	("M-g !" . consult-flycheck)))

;;;;; Enable project.el integration
(use-package consult-project-extra
  :bind
  (("C-c p f" . consult-project-extra-find)
   ("C-c p o" . consult-project-extra-find-other-window)))

;;;;; Enable yasnippet integration
(use-package consult-yasnippet
  :bind ([remap yas-insert-snippet] . consult-yasnippet))

;;;;; Enable LSP integration
(use-package consult-lsp
  :bind (:map lsp-command-map
	      ("g !" . consult-lsp-diagnostics)
	      ("g ."   . consult-lsp-symbols)))

;;;; Setup which-key
(use-package which-key
  :hook
  after-init
  :custom
  (which-key-idle-delay 2))

;;;; Setup modeline

;;;;; Disable modeline for some buffers
(use-package hide-mode-line
  :hook
  help-mode
  flycheck-error-list-mode
  vterm-mode)

;;;; Setup Treemacs
(use-package treemacs
  :hook
  (prog-mode . treemacs-project-follow-mode)
  :bind
  ("C-c t t" . treemacs)
  
  :custom
  (treemacs-no-png-images t)
  
  :config
  (treemacs-filewatch-mode)
  (treemacs-git-mode 'deferred)
  (treemacs-git-commit-diff-mode))

;;;; Enable tabs
(use-package centaur-tabs
  :hook
  ;; Disable tabs in some buffers
  ((dired-mode
    vterm-mode
    dashboard-mode
    vc-dir-mode
    help-mode
    flycheck-error-list-mode)

   . centaur-tabs-local-mode)

  (centaur-tabs-mode . centaur-tabs-headline-match)
  
  after-init
  
  :custom
  (centaur-tabs-style "alternate")
  (centaur-tabs-height 36)
  (centaur-tabs-set-icons nil)
  (centaur-tabs-set-close-button nil)
  (centaur-tabs-set-modified-marker t)
  (centaur-tabs-cycle-scope 'tabs)
  
  :config
  (centaur-tabs-headline-match)
  (centaur-tabs-mode)
  
  :bind
  ;; Buffer navigation
  ("C-x C-<right>" . centaur-tabs-forward)
  ("C-x C-<left>" . centaur-tabs-backward)
  ("C-x C-<up>" . centaur-tabs-forward-group)
  ("C-x C-<down>" . centaur-tabs-backward-group)
  ("C-<tab>" . centaur-tabs-forward)
  ("C-<iso-lefttab>" . centaur-tabs-backward))

;;;; Setup window navigation
(use-package windmove
  :bind
  ("C-c C-<up>" . windmove-up)
  ("C-c C-<down>" . windmove-down)
  ("C-c C-<left>" . windmove-left)
  ("C-c C-<right>" . windmove-right))

;;;; Use popup windows
(use-package popwin
  :hook after-init
  :config
  (add-to-list 'popwin:special-display-config
	       '("*Help*"
		 :position right :width 0.45 :stick t)))

;;;; Add support for Zen mode
(use-package olivetti
  :bind
  ("C-c t z" . olivetti-mode))

;;;; Use undo history for humans
(use-package undo-fu
  :bind
  ([remap undo] . undo-fu-only-undo)
  ([remap redo] . undo-fu-only-redo))

;;;;; Save undo history
(use-package undo-fu-session
  :hook
  (after-init . global-undo-fu-session-mode)

  :custom
  (undo-fu-session-directory (locate-user-emacs-file "undo-history"))
  (undo-fu-session-linear t)
  (undo-fu-session-file-limit 512))

;;;; Allow to profile Emacs startup
(use-package esup
  :commands esup)

;;;; Use XClip if running in terminal
(use-package xclip
  :if (not (display-graphic-p))
  :hook after-init)

;;;; Use Tree Sitter for syntax highlighting
(use-package tree-sitter
  :hook
  ;; Use this hack to defer loading
  (dashboard-after-initialize . global-tree-sitter-mode)
  :config
  (add-hook 'tree-sitter-after-on-hook 'tree-sitter-hl-mode))

(use-package tree-sitter-langs
  :after tree-sitter)

;;;; Enable workspaces
(use-package perspective
  :hook
  (after-init . persp-mode)
  
  :custom
  (persp-mode-prefix-key (kbd "C-c TAB"))
  
  :config
  (consult-customize consult--source-buffer :hidden t :default nil)
  (add-to-list 'consult-buffer-sources persp-consult-source))

(use-package treemacs-perspective)

;;;; Setup Git integration
(use-package magit)
;;;; Org-mode setup
(with-eval-after-load 'ox-latex
  (setq-default org-export-with-toc nil
                org-latex-default-packages-alist '(("AUTO" "inputenc" t
                                                    ("pdflatex"))
                                                   ("T2A" "fontenc" t
                                                    ("pdflatex"))
                                                   (#1="" "graphicx" t)
                                                   (#1# "longtable" nil)
                                                   (#1# "wrapfig" nil)
                                                   (#1# "rotating" nil)
                                                   ("normalem" "ulem" t)
                                                   (#1# "amsmath" t)
                                                   (#1# "amssymb" t)
                                                   (#1# "capt-of" nil)
                                                   (#1# "hyperref" nil)
                                                   ("russian" "babel" t))))

(with-eval-after-load 'org
  (bind-keys
   :map org-mode-map
   ("C-c m" . (lambda ()
                (interactive)
                (insert "\\[ ")
                (save-excursion
                  (insert " \\]"))))
   ("C-c d" . (lambda ()
                (interactive)
                (let ((line-content
                       (thing-at-point 'line)))
                  (save-excursion
                    (end-of-line)
                    (newline)
                    (insert line-content))
                  (next-line)))))
  (setq-default org-support-shift-select t))

;;;; LaTeX setup
(use-package yasnippet
  :hook
  (latex-mode . yas-minor-mode))

(setq org-highlight-latex-and-related '(latex script entities))

(with-eval-after-load 'org
  (bind-keys :map org-mode-map
             ("C-c e" . org-latex-export-to-pdf))
  (advice-add 'org-latex-export-to-pdf :before (lambda (&rest ARGS)
                                                 (let ((save-silently t))
                                                   (save-buffer))))
  (add-to-list 'org-preview-latex-process-alist
               '(dvipng
                 :programs ("latex" "dvipng")
                 :description "dvi > png"
                 :message "you need to install the programs: latex and dvipng."
                 :image-input-type "dvi"
                 :image-output-type "png"
                 :image-size-adjust (1.7 . 1.7)
                 :latex-compiler
                 ("latex -interaction nonstopmode -output-directory %o %f")
                 :image-converter
                 ("dvipng -D %D -T tight -bg Transparent -o %O %f"))))

;;; Footer

(provide 'init)
;;; init.el ends here.
